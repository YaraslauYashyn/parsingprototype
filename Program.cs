﻿using OpenQA.Selenium;
using Parser.Utils;
using Parser.WebPages;
using System;

namespace Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            var _mvideoPagePage = PageFactory.Instance.Get<MVideoPage>();

            _mvideoPagePage.Open();

            if (!_mvideoPagePage.IsSearchFieldPresent)
                Console.WriteLine("Поисковая строка недоступна");

            var itemForSearch = "IPhone 12";

            _mvideoPagePage.SearchField.SendKeys(itemForSearch);

            _mvideoPagePage.SearchField.SendKeys(Keys.Enter);

            var prices = _mvideoPagePage.GetAllPricesParagraphs();

            Console.WriteLine("На первой странице по заданному товару были найдены следующие цены:");

            foreach (var price in prices)
            {
                Console.WriteLine(price.Text);
            }

            _mvideoPagePage.Close();
        }
    }
}
