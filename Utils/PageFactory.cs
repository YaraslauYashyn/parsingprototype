﻿using OpenQA.Selenium;
using Parser.WebPages;
using System;

namespace Parser.Utils
{
    public class PageFactory: IDisposable
    {
        private static IWebDriver _driver;

        private PageFactory() { }

        private static readonly Lazy<PageFactory> _lazy = new Lazy<PageFactory>(() => new PageFactory());

        public static PageFactory Instance => _lazy.Value;

        public T Get<T>() where T : AbstractPage
        {
            OpenBrowser();
            object[] args = { _driver };
            return (T)Activator.CreateInstance(typeof(T), args);
        }
        public static void OpenBrowser()
        {
            _driver = DriverFactory.GetDriver();
        }

        public static void CloseBrowser()
        {
            _driver.Close();
        }

        public void Dispose()
        {
            CloseBrowser();
        }
    }
}
