﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Parser.Settings;
using System;

namespace Parser.Utils
{
    public static class DriverFactory
    {
        private static ChromeDriver Driver { get; set; }
        public static IWebDriver GetDriver()
        {
            if (Driver == null)
            {
                Driver = new ChromeDriver();
                Driver.Manage().Window.Maximize();

                Driver.Manage().Timeouts().PageLoad =
                    TimeSpan.FromMilliseconds(MainSettings.PageLoad);
                Driver.Manage().Timeouts().ImplicitWait =
                    TimeSpan.FromMilliseconds(MainSettings.ImplicitWait);
                Driver.Manage().Timeouts().AsynchronousJavaScript =
                    TimeSpan.FromMilliseconds(MainSettings.AsyncJS);
            }

            return Driver;
        }
    }
}
