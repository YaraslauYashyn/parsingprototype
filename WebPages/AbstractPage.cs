﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Parser.WebPages
{
    public abstract class AbstractPage
    {
        protected IWebDriver _driver;

        protected AbstractPage(IWebDriver driver)
        {
            _driver = driver;
        }

        protected IWebElement FindByCss(string css) => _driver.FindElement(By.CssSelector(css));

        protected IAlert GetAlert() => _driver.SwitchTo().Alert();

        public IReadOnlyCollection<IWebElement> GetAllParagraphsByClass(string className) => _driver.FindElements(By.ClassName(className));

        protected bool IsElementPresent(By by)
        {
            try
            {
                _driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        protected bool IsAlertPresent()
        {
            try
            {
                _driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }
    }
}
