﻿using OpenQA.Selenium;
using Parser.Settings;
using Parser.Timeouts;
using System.Collections.Generic;

namespace Parser.WebPages
{
    public class MVideoPage : AbstractPage
    {
        public MVideoPage(IWebDriver driver) : base(driver) { }

        public IWebDriver Driver
        {
            get
            {
                CustomTimeout.ShortWait();
                return _driver;
            }

            set => _driver = value;
        }

        public bool IsSearchFieldPresent => IsElementPresent(By.Id("header-search-input"));

        public IWebElement SearchField => FindByCss("#header-search-input");

        public IReadOnlyCollection<IWebElement> GetAllPricesParagraphs() => GetAllParagraphsByClass("price__main-value");

        public void Open()
        {
            Driver.Url = MainSettings.MVideoPageUrl;
        }

        public void Close()
        {
            Driver.Close();
        }
    }
}
